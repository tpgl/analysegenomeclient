using namespace std;

#pragma once
#include <list>
class Server
{
public:
	Server();
	~Server();
	
	void Envoi();
	void Reception();
	void DemandeListeMaladie();

protected:
	CString IP;
	CString port;
	list<Server> serverList;
	
};

