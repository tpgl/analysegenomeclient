#pragma once
#include <list>
#include <utility>
#include <istream>
#include "Maladie.h"
#include <string>
class Resultats
{
public:
	Resultats();
	~Resultats();
private:
	void LoadListeMaladie(const std::string);

	std::list<std::pair<Maladie, bool>> listeMaladie;
	
};

