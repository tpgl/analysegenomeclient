#pragma once
#include <string>
class Maladie
{
public:
	Maladie();
	Maladie(std::string);
	~Maladie();
	std::string GetNom();
private:
	std::string nom;
};

