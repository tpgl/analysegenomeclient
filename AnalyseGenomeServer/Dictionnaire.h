#pragma once
#include <map>
#include <string>
#include "MaladieS.h"
class Dictionnaire
{
public:
	Dictionnaire();
	~Dictionnaire();
private:
	std::map<std::string, MaladieS> listeMaladies;
};

